// Fill out your copyright notice in the Description page of Project Settings.


#include "UIPushkinStudioTest/Public/JsonLibrary.h"
#include "JsonUtilities/Public/JsonUtilities.h"
#include "Serialization/JsonReader.h"
#include "Misc/DateTime.h"

static const FString RootName("Index");
static const FString WritePath(FPaths::ProjectSavedDir() / "Json");
static const FString IndexFileName("index.json");
static const FString Tour8FileName("tour8.json");
static const FString FilepathFull(WritePath / IndexFileName);

bool UJsonLibrary::ReadDataTournaments(TArray<FString>& Names, TArray<FString>& Links)
{
	JsonObjectPtr JsonRootObject = ReadJsonFile(FilepathFull);

	if (JsonRootObject)
	{
		//CharName = JsonRootObject->GetStringField("Character");
		
		/*for (TSharedPtr<FJsonValue> V : JsonRootObject->GetArrayField("tournaments"))
		{
			Tournaments.Add(V->AsString());
		}*/

		TArray<JsonValuePtr> ArrTournaments = JsonRootObject->GetArrayField("tournaments");
		for (int32 index = 0; index < ArrTournaments.Num(); index++)
		{
			JsonObjectPtr obj = ArrTournaments[index]->AsObject();
		
			Names.Add(obj->GetStringField("name"));

			Links.Add(obj->GetStringField("link"));
		}

		return true;
	}
	return false;
}

bool UJsonLibrary::ReadDataTour8(FString Link, TArray<FString>& Names, TArray<FString>& Images, TArray<FString>& Team0, TArray<FString>& Team1, TArray<int>& Winners, TArray<int>& Timestamp, TArray<int>& Rounds)
{
	FString FilepathFullTour8(WritePath / Link);

	JsonObjectPtr JsonRootObject = ReadJsonFile(FilepathFullTour8);

	if (JsonRootObject)
	{

		TArray<JsonValuePtr> ArrTeams = JsonRootObject->GetArrayField("teams");
		for (int32 index = 0; index < ArrTeams.Num(); index++)
		{
			JsonObjectPtr obj = ArrTeams[index]->AsObject();

			Names.Add(obj->GetStringField("name"));

			Images.Add(obj->GetStringField("image"));
		}

		TArray<JsonValuePtr> ArrMatches = JsonRootObject->GetArrayField("matches");
		for (int32 index = 0; index < ArrMatches.Num(); index++)
		{
			JsonObjectPtr obj = ArrMatches[index]->AsObject();

			Team0.Add(obj->GetStringField("team0"));

			Team1.Add(obj->GetStringField("team1"));

			Winners.Add(obj->GetIntegerField("winner"));

			Timestamp.Add(obj->GetIntegerField("timestamp"));

			Rounds.Add(obj->GetIntegerField("round"));
		}

		return true;
	}
	return false;
}

JsonObjectPtr UJsonLibrary::ReadJsonFile(FString FullPath)
{
	FString RawData;
	bool bLoadedFile = FFileHelper::LoadFileToString(RawData, *FullPath);
	JsonObjectPtr JsonRootObject = MakeShareable(new FJsonObject());

	if (bLoadedFile)
	{
		TSharedRef<TJsonReader<>> JsonReader = TJsonReaderFactory<>::Create(RawData);
		FJsonSerializer::Deserialize(JsonReader, JsonRootObject);
	}
	return JsonRootObject;
}
