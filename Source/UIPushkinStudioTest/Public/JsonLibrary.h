// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "Dom/JsonObject.h"
#include "JsonLibrary.generated.h"

typedef TSharedPtr<FJsonObject> JsonObjectPtr;
//using JsonObjectPtr_t = TSharedPtr<FJsonObject>;
typedef TSharedPtr<FJsonValue> JsonValuePtr;

UCLASS()
class UIPUSHKINSTUDIOTEST_API UJsonLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

	UFUNCTION(BlueprintCallable, Category = "Temp")
	static bool ReadDataTournaments(TArray<FString>& Names, TArray<FString>& Links);

	UFUNCTION(BlueprintCallable, Category = "Temp")
	static bool ReadDataTour8(FString Link, TArray<FString>& Names, TArray<FString>& Images, TArray<FString>& Team0, TArray<FString>& Team1, TArray<int>& Winners, TArray<int>& Timestamp, TArray<int>& Rounds);

private:

	static JsonObjectPtr ReadJsonFile(FString FullPath);
};
